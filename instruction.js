Goal:
	a. customer registration 
	b. add product 
	c. update product price 
	d. archive product 
	e. add to cart 
	f. show cart 
	g. update cart quantity 
	h. clear cart 
	i. compute cart total 
	j. checkout

Requirements:
	1. Customer class: 
		a. email property - string
		b. cart property - instance of Cart class
		c. orders property - array of objects with structure {products: cart contents, totalAmount: cart total}
		d. checkOut() method - pushes the Cart instance to the orders array if Cart contents is not empty

	2. Product class:
		a. name property - string
		b. price property - number
		c. isActive property - Boolean: defaults to true
		d. archive() method - will set isActive to false if it is true to begin with
		e. updatePrice() method - replaces product price with passed in numerical value

	3. Cart class:
		a. contents property - array of objects with structure: {product: instance of Product class, quantity: number
		}	
		b. totalAmount property -number
		c. addToCart() method- accepts a Product instance and a quantity number as arguments, pushes an object with structure: {product: instance of Product class, quantity: number} to the contents property
		d. showCartContents() method - logs the contents property in the console
		e. updateProductQuantity() method - takes in a string used to find a product in the cart by name, and a new quantity. Replaces the quantity of the found product in the cart with the new value.
		f. clearCartContents() method - empties the cart contents
		g. computeTotal() method - iterates over every product in the cart, multiplying product price with their respective quantities. Sums up all results and sets value as totalAmount.	

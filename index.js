//console.log("Capstone is waving");

// C U S T O M E R   C L A S S
class Customer {
	constructor(email){
		if(typeof email === 'string') {
			this.email = email;
		}
		else {
			this.email = undefined;
		}

		this.cart = new Cart();
		this.orders = [];
	}

	checkOut(){
		if(this.cart.contents.length == 0 || this.cart.contents.length == null){
			console.log("You haven't added any products in the cart yet")
		}
		else {
			this.orders.push({products: this.cart.contents ,totalAmount: this.cart.computeTotal().totalAmount})
			this.cart.contents = [];
			this.cart.totalAmount = 0;
		}
		return this;
	}
}


// P R O D U C T   C L A S S 
class Product {
	constructor(name, price){
		if(typeof name === 'string'){
			this.name = name;
		}
		else {
			this.name = undefined;
		}

		if(typeof price === 'number'){
			this.price = price;
		}
		else {
			this.price = undefined;
		}

		this.isActive = true;
	}

	archive(){
		this.isActive = false;
		console.log(`${this.name} is currently unavailable`);
	}

	updatePrice(updatedPrice){
		this.price = updatedPrice;
		console.log(`${this.name} changed its price to ${updatedPrice}`)
		return this;
	}
}


// C A R T   C L A S S 
class Cart {
	constructor(){
		this.contents = [];
		this.totalAmount = 0;
	}

	addToCart(product, quantity){	
		if(product.isActive){
			this.contents.push({product, quantity})
		}
		else {
			console.log("Sorry, this product is currently unavailable")
		}
		return this;
	}

	showCartContents(){
		if(this.contents.length > 0){
			console.log(this.contents);
		}
		else {
			console.log("You haven't added any products in the cart yet");
		}	
		return this;
	}

	updateProductQuantity(name, quantity){
		if(this.contents.length > 0){
			this.contents.forEach(content => {
				if(content.product.name === name){
					content.quantity = quantity;
					console.log(`Quantity has been updated`);			
				}
			})
		}
		else{
			console.log("You haven't added any products in the cart yet");
		}
		return this;
	}

	clearCartContents(){
		this.contents = [];
		this.totalAmount = 0;
		return this;
	}

	computeTotal(){
		let totalPrice = 0;
		if(this.contents.length > 0){
			this.contents.forEach(content => {
				totalPrice = totalPrice + content.quantity * content.product.price; 
			});

			this.totalAmount = totalPrice;
		}
		else{
			console.log("You haven't added any products in the cart yet");
		}
		return this;
	}
}

// Instantiate customer 
const shan = new Customer("shan@mail.com");

// Instantiate products 
const productA = new Product("Charger", 300);
const productB = new Product("Headphone", 400);
const productC = new Product("OTG", 100);
const productD = new Product("Flashdrive", 600);

// Statements for testing

// Archive a product
productA.archive();

// Updating product's price
productB.updatePrice(500)

// Add products to the cart
shan.cart.addToCart(productA, 1)
shan.cart.addToCart(productB, 2)
shan.cart.addToCart(productC, 3)
shan.cart.addToCart(productD, 4)

// Show cart's contents
shan.cart.showCartContents()

// Updating product's quantity
shan.cart.updateProductQuantity('Headphone', 1)

// Clear cart's contents
shan.cart.clearCartContents()

// Compute sum of ordered products
shan.cart.computeTotal()

// Checkout
shan.checkOut()

